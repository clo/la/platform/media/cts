This folder contains media files for CTS media stress test.
The video clip, Big Buck Bunny is copyrighted 2008 by Blender Foundation / www.bigbuckbunny.org under Creative Commons Attribution 3.0 license.
The original clip from www.bigbuckbunny.org is converted to various formats using ffmpeg (ffmpeg.org).

These media clips should be copied to Device Under Test before running the CTS test. Here is the instruction for the manual copy.
1. connect the device under test via ADB.
2. chmod 544 copy_media.sh
3. run copy_media.sh
  For default resolution, just run ./copy_media.sh
  For max resolution of 720x480, run ./copy_media.sh 720x480
  If you are not sure about the maximum resolution of the video playback in the device, try 1920x1080 so that all files are copied.
  If there are multiple devices under adb, add -s serial option to the end. For example, to down-load up to 720x480 to device with serial 1234567, run copy_media.sh 720x480 -s 1234567
