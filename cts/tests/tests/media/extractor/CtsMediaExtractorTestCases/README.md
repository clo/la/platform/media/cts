# Files for CtsMediaExtractorTestCases

This folder contains media files for CtsMediaExtractorTestCases.

cts-tradefed automatically downloads and copies these files to the device.

These media files can also be copied to Device Under Test before running the CTS,  
in which case cts-tradefed uses the files present on the device.

Here are the instructions for copying these files manually to the device.

```
$ connect the device under test via ADB.
$ chmod a+x copy_media.sh
$ ./copy_media.sh
```

If there are multiple devices connected under adb, add -s serial option to ./copy_media.sh

### Big Buck Bunny details
--------------------------------------------------------------

The video clip, Big Buck Bunny is copyrighted 2008 by Blender Foundation / www.bigbuckbunny.org under Creative Commons Attribution 3.0 license.

The original clip from www.bigbuckbunny.org is converted to various formats using ffmpeg (ffmpeg.org).
