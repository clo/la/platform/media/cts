# Files for CtsMediaV2TestCases

This folder contains media files for CtsMediaV2TestCases.

cts-tradefed automatically downloads and copies these files to the device.

These media files can also be copied to Device Under Test before running the CTS,  
in which case cts-tradefed uses the files present on the device.

Here are the instructions for copying these files manually to the device.

```
$ connect the device under test via ADB.
$ chmod a+x copy_media.sh
$ ./copy_media.sh
```

If there are multiple devices connected under adb, add -s serial option to ./copy_media.sh

### Big Buck Bunny details
--------------------------------------------------------------

Attribution: (c) copyright 2008, Blender Foundation / www.bigbuckbunny.org
License: Creative Commons Attribution 3.0 license.
Download link: https://peach.blender.org/download/

The original clip is converted to various formats using ffmpeg (ffmpeg.org).


### Cosmos Laundromat
--------------------------------------------------------------

Attribution: (CC) Blender Foundation | gooseberry.blender.org
License: Creative Commons Attribution 3.0 license.
Download link: https://media.xiph.org/cosmoslaundromat/Cosmos_Laundromat_1-2k24p-HDR_P3PQ-exr/

The original clip is converted to various formats using ffmpeg (ffmpeg.org).
