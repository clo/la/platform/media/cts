#!/bin/bash
#
# build a current zip file
#
# parameters

if [ ! -f ./parameters ] ; then
    echo Missing parameter file parameters
    exit 1
fi
source ./parameters

if [ "${DIR}" = "" ] ; then
    echo Missing DIR from parameters
    exit 1
fi
if [ "${VERSION}" = "" ] ; then
    echo Missing VERSION from parameters
    exit 1
fi

if [ ! -d "${DIR}" ] ; then
    echo Missing extractor test data directory: ${DIR}
    exit 1
fi

# we store locally in "foo", but we want to build a "foo-version.zip"
# and have it unpack in "foo-version".  The symlink game does this for us.
#
# -- probably some better error/exit strategy than this

rm -f ${DIR}-${VERSION}.zip || exit 1
rm -f ${DIR}-${VERSION} || exit 1
ln -s ${DIR} ${DIR}-${VERSION} || exit 1
zip -r ${DIR}-${VERSION}.zip ${DIR}-${VERSION} || exit 1

# clean up our symlink contrivance
rm -f ${DIR}-${VERSION} || exit 1

echo all good
