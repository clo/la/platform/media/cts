#!/bin/bash
#
# build a current zip file
#
# parameters

if [ ! -f ./parameters ] ; then
    echo Missing parameter file parameters
    exit 1
fi
source ./parameters

if [ "${DIR}" = "" ] ; then
    echo Missing DIR from parameters
    exit 1
fi
if [ "${VERSION}" = "" ] ; then
    echo Missing VERSION from parameters
    exit 1
fi

if [ ! -d "${DIR}" ] ; then
    echo Missing extractor test data directory: ${DIR}
    exit 1
fi

rm -f ${DIR}-${VERSION}.zip
(cd ./${DIR} && zip -r ../${DIR}-${VERSION}.zip .)

echo all good
